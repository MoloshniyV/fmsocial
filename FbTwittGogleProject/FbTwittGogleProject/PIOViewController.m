//
//  PIOViewController.m
//  FbTwittGogleProject
//
//  Created by Admin on 10/4/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "PIOViewController.h"
#import "FMSocialNetworkVC.h"
static const NSString *kReadFacebookFriendListPermission = @"read_friendlists";
static const NSString *kEMailFacebookPermission = @"email";
static const NSString *kUserLocationFacebookPermission = @"user_location";
static const NSString *kFriendsLocationFacebookPermission = @"friends_location";
static const NSString *kFriendsPhotosFacebookPermission = @"friends_photos";
static const NSString *kPublishStreamFacebookPermission = @"publish_stream";

@interface PIOViewController ()
@property (strong, nonatomic)FMSocialNetworkVC *fmSocial;
@end

@implementation PIOViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (IBAction)twitterWasPressed:(id)sender
{
    _fmSocial = [[FMSocialNetworkVC alloc] init];
    [_fmSocial didPressTwitterButton:nil];

}
- (IBAction)faceBookWasPressed:(id)sender
{
    _fmSocial = [[FMSocialNetworkVC alloc] init];
    [_fmSocial loginFaceBookWithPermissions:[NSArray arrayWithObjects:nil] accessToken:^(NSString *accessToken)
    {
        NSLog(@"%@",accessToken);
    }];
}
- (IBAction)googlePluskWasPressed:(id)sender
{
    _fmSocial = [[FMSocialNetworkVC alloc] init];
    [_fmSocial didPressGooglePlusButton:nil];
}

@end
