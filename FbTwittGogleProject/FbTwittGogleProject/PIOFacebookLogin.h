//
//  FMSocialFacebookLogin.h
//  FbTwittGogleProject
//
//  Created by Admin on 10/8/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PIOFacebookLogin : NSObject

@property(copy, nonatomic, readonly)NSString *facebookAccessToken;

- (void)loginFaceBookWithPermissions:(NSArray*)permissions accessToken:(void(^)(NSString *accessToken))accessToken;

@end
