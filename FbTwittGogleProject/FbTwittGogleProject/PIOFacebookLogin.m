//
//  FMSocialFacebookLogin.m
//  FbTwittGogleProject
//
//  Created by Admin on 10/8/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "PIOFacebookLogin.h"
#import <FacebookSDK/FacebookSDK.h>

static const NSString *kplFaceBookAppId = @"FacebookAppID";

@interface PIOFacebookLogin()

- (void)sessionStateChanged:(FBSession *)theSession state:(FBSessionState)theState error:(NSError *)theError;
- (BOOL)isPlistContainFaceBookAppID;

@end

@implementation PIOFacebookLogin

- (void)loginFaceBookWithPermissions:(NSArray*)permissions accessToken:(void (^)(NSString *))accessToken
{
    if (![self isPlistContainFaceBookAppID])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information" message:@"add FacebookAppID KEY to Plist. For more information look here: https://developers.facebook.com/docs/ios/getting-started/" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alertView show];
        accessToken(nil);
    }
    else
    {
        FBSession *activeSession = [FBSession activeSession];
        
        __block typeof(self) wSelf = self;
        
        if ([activeSession isOpen])
        {
            _facebookAccessToken = [[activeSession accessTokenData] accessToken];
            accessToken(_facebookAccessToken);
        }
        else
        {
            
            FBSession *session = [[FBSession alloc] initWithPermissions:permissions];
            [FBSession setActiveSession:session];
            [FBSession.activeSession openWithCompletionHandler:^(FBSession *theSession, FBSessionState theState, NSError *theError)
            {
                [wSelf sessionStateChanged:theSession
                                    state:theState
                                    error:theError];
                accessToken([self facebookAccessToken]);
            }];
            
        }
    }
}

- (BOOL)isPlistContainFaceBookAppID
{
    NSBundle* bundle = [NSBundle mainBundle];
    
    if   (bundle.infoDictionary[kplFaceBookAppId] != nil)
    {
        return YES;
    }
    
    return NO;
}
- (void)sessionStateChanged:(FBSession *)theSession state:(FBSessionState)theState error:(NSError *)theError
{
    switch (theState)
    {
        case FBSessionStateOpen:
            _facebookAccessToken = [[theSession accessTokenData] accessToken];
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            [FBSession setActiveSession:nil];
           
            break;
        default:
            break;
    }
    
    if (theError)
    {
        [[[UIAlertView alloc] initWithTitle:@"Facebook session error"
                                    message:[NSString stringWithFormat:@"%@, Session state - %i", theError.localizedDescription, theState]
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

#pragma mark FBSession delegate

- (void)fbDidNotLogin:(BOOL)cancelled
{

}

- (void)fbDidLogin
{

}
@end
