//
//  FMSocialNetworkVC.h
//  FarmerApp
//
//  Created by Shakhmin Aleksandr on 4/18/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//
#import "PIOFacebookLogin.h"

@class GPPSignInButton;

@interface FMSocialNetworkVC : UIViewController

@property(copy, nonatomic,readonly)NSString *facebookAccessToken;

- (IBAction)didPressTwitterButton:(id)theSender;
- (IBAction)didPressGooglePlusButton:(id)theSender;

- (void)didLoggedInTwitterWithAuthToken:(NSString *)theAccessToken authSecret:(NSString *)theAuthSecret;
- (void)didLoggedInGooglePlusWithAuthToken:(NSString *)theAccessToken;



- (void)loginFaceBookWithPermissions:(NSArray*)permissions accessToken:(void(^)(NSString *accessToken))accessToken;
- (void)loginTwitter;

@end