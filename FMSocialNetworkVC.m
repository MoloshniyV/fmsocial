//
//  FMSocialNetworkVC.m
//  FarmerApp
//
//  Created by Shakhmin Aleksandr on 4/18/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//


static NSString * const kClientId = @"327241834971.apps.googleusercontent.com";

const NSInteger kActionSheetButtonIndexNone = -1;

#import "FMSocialNetworkVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Accounts/Accounts.h>
#import "OAuth+Additions.h"
#import "TWAPIManager.h"
#import "TWSignedRequest.h"
#import "MBProgressHUD.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>

#import "PIOFacebookLogin.h"
@interface FMSocialNetworkVC ()<UIActionSheetDelegate, GPPSignInDelegate>

@property(nonatomic, strong) ACAccountStore *accountStore;
@property(nonatomic, strong) TWAPIManager *apiManager;
@property(nonatomic, strong) NSArray *accounts;

@property(nonatomic, strong)PIOFacebookLogin *socialFacebookLogin;

@end

@implementation FMSocialNetworkVC

- (id)init
{
    self = [super init];
    
    if (self != nil)
    {
        [self refresh];
        [self setSocialFacebookLogin:[[PIOFacebookLogin alloc] init]];
    }
    
    return self; 
}

- (void)loginFaceBookWithPermissions:(NSArray*)permissions accessToken:(void(^)(NSString *accessToken))accessToken
{
    [[self socialFacebookLogin] loginFaceBookWithPermissions:permissions accessToken:accessToken];
}

- (NSString *)facebookAccessToken
{
    return [[self socialFacebookLogin] facebookAccessToken];
}

- (void)refresh
{
    _accountStore = [[ACAccountStore alloc] init];
    _apiManager = [[TWAPIManager alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTwitterAccounts:)
                                                 name:ACAccountStoreDidChangeNotification
                                               object:[NSNumber numberWithBool:YES]];
    
    [[GPPSignIn sharedInstance] setDelegate:self];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - IBActions

- (IBAction)didPressTwitterButton:(id)theSender
{
    [self refreshTwitterAccounts:NO];
}

- (IBAction)didPressGooglePlusButton:(id)theSender
{
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.clientID = kClientId;
    
    if (signIn.clientID == nil)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information" message:@"ClientID is nil. For more information look here: https://developers.google.com/+/mobile/ios/getting-started" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alertView show];
   
    }
    else
    {
        GTMOAuth2Authentication *authentication = [signIn authentication];
       
        
        if (authentication == nil)
        {
            [signIn authenticate];
        }
        else
        {
            [self didLoggedInGooglePlusWithAuthToken:[authentication accessToken]];
        }

    }
}


#pragma mark - Google plus sign in delegate

- (void)finishedWithAuth:(GTMOAuth2Authentication *)theAuthentication error:(NSError *)theError
{
    if (theError == nil)
    {
        [self didLoggedInGooglePlusWithAuthToken:[theAuthentication accessToken]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[theError localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - Twitter

- (void)refreshTwitterAccounts:(BOOL)theShouldAuthenticate
{
    //  Get access to the user's Twitter account(s)
    
    [self obtainAccessToAccountsWithBlock:^(BOOL theGranted, NSError *theError)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            if (theGranted)
            {
                if (!theShouldAuthenticate)
                {
                    [self performReverseAuth];
                }
            }
            else
            {
                if (theError)
                {
                    if ([theError code] == ACErrorAccountNotFound)
                    {
                        [[[UIAlertView alloc] initWithTitle:@"No Accounts"
                                                    message:@"Please configure a Twitter "
                          "account in Settings.app"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil] show];
                    }
                    else
                    {
                        [[[UIAlertView alloc] initWithTitle:@"Login via Twitter failed"
                                                    message:@"Please check/configure a Twitter "
                          "account in Settings.app"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil] show];
                    }
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:@""
                                                message:@"You were not granted access to the Twitter accounts."
                                               delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil] show];
                }
            }
        });
    }];
}

- (void)obtainAccessToAccountsWithBlock:(void (^)(BOOL theGranted, NSError *theError))theBlock
{
    ACAccountType *twitterType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    __block typeof(self) wSelf = self;
    
    ACAccountStoreRequestAccessCompletionHandler handler = ^(BOOL theGranted, NSError *theInternalError)
    {
        if (theInternalError)
        {
            NSLog(@"failed to get permissions - %@", theInternalError);
        }
        
        if (theGranted)
        {
            [wSelf setAccounts:[_accountStore accountsWithAccountType:twitterType]];
        }
        
        theBlock(theGranted, theInternalError);
    };
    
    //  This method changed in iOS6.  If the new version isn't available, fall
    //  back to the original (which means that we're running on iOS5+).
 

    NSLog(@"%d",[_accountStore
                 respondsToSelector:@selector(requestAccessToAccountsWithType:
                                              options:
                                              completion:)]);
    NSLog(@"%d",[_accountStore
                 respondsToSelector:@selector(requestAccessToAccountsWithType:withCompletionHandler:)]);


    if ([_accountStore
         respondsToSelector:@selector(requestAccessToAccountsWithType:
                                      options:
                                      completion:)])
    {
        [_accountStore requestAccessToAccountsWithType:twitterType
                                               options:nil
                                            completion:handler];
    }
    else
    {
        [_accountStore requestAccessToAccountsWithType:twitterType
                                 withCompletionHandler:handler];
    }
}

- (void)performReverseAuth
{
    if ([TWAPIManager isLocalTwitterAccountAvailable])
    {
        UIActionSheet *sheet = [[UIActionSheet alloc]
                                initWithTitle:@"Choose an Account"
                                delegate:self
                                cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                otherButtonTitles:nil];
        
        for (ACAccount *acct in _accounts)
        {
            [sheet addButtonWithTitle:acct.username];
        }
        
        [sheet addButtonWithTitle:@"Cancel"];
        [sheet setDestructiveButtonIndex:[_accounts count]];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [self tabBarController] != nil)
        {
            [sheet showFromTabBar:[[self tabBarController] tabBar]];
        }
        else
        {
            [sheet showInView:[self view]];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"No Accounts"
                              message:@"Please configure a Twitter "
                              "account in Settings.app"
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)theActionSheet clickedButtonAtIndex:(NSInteger)theButtonIndex
{
    if ([TWAPIManager hasAppKeys])
    {
        if (theButtonIndex > kActionSheetButtonIndexNone && theButtonIndex < (theActionSheet.numberOfButtons - 1))
        {
            UIWindow *mainWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
            [MBProgressHUD showHUDAddedTo:mainWindow animated:YES];

            __block typeof(self) wSelf = self;
            
            [_apiManager performReverseAuthForAccount:_accounts[theButtonIndex]
                                          withHandler:^(NSData *theResponseData, NSError *theError)
                                          {
                                              [MBProgressHUD hideHUDForView:mainWindow animated:YES];
                                              
                                              if (theResponseData && !theError)
                                              {
                                                  NSString *responseStr = [[NSString alloc] initWithData:theResponseData encoding:NSUTF8StringEncoding];
                                                 
                                                  NSArray *parts = [responseStr componentsSeparatedByString:@"&"];
                                                 
                                                  NSString *token = nil;
                                                  NSString *tokenSecret = nil;
                                                 
                                                  for (NSString *part in parts)
                                                  {
                                                      if ([part rangeOfString:@"oauth_token_secret"].location != NSNotFound)
                                                      {
                                                          tokenSecret = [part stringByReplacingOccurrencesOfString:@"oauth_token_secret=" withString:@""];
                                                      }
                                                      else if ([part rangeOfString:@"oauth_token"].location != NSNotFound)
                                                      {
                                                          token = [part stringByReplacingOccurrencesOfString:@"oauth_token=" withString:@""];
                                                      }
                                                  }
                                                 
                                                  NSLog(@"TWITTER TOKENS: %@\n\n%@\n%@", [parts componentsJoinedByString:@"\n"], token, tokenSecret);
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^
                                                  {
                                                      [wSelf didLoggedInTwitterWithAuthToken:token authSecret:tokenSecret];
                                                  });
                                              }
                                              else
                                              {
                                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can't login via twitter"
                                                                                                  message:@"Please configure a Twitter "
                                                                                                           "account in Settings.app"
                                                                                                 delegate:nil
                                                                                        cancelButtonTitle:@"OK"
                                                                                        otherButtonTitles:nil];
                                                  [alert show];
                                                     
                                                  NSLog(@"Error!\n%@", [theError localizedDescription]);
                                              }
                                          }];
        }
        
    }
    else
    {
       UIAlertView *view = [[UIAlertView alloc] initWithTitle:@"Information" message:@" Please add TWITTER_CONSUMER_SECRET and  TWITTER_CONSUMER_KEY to you Plist  with corresponding value which you can find in  https://dev.twitter.com/apps" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [view show];
    }
}

#pragma mark - Open social networks friends list

- (void)didLoggedInFacebookWithAuthToken:(NSString *)theAccessToken
{
    NSLog(@"%@",theAccessToken);
}

- (void)didLoggedInTwitterWithAuthToken:(NSString *)theAccessToken authSecret:(NSString *)theAuthSecret
{
    NSLog(@"overwrite me");
}

- (void)didLoggedInGooglePlusWithAuthToken:(NSString *)theAccessToken
{
    NSLog(@"overwrite me");
}

@end